const nodemailer = require('nodemailer');
const pug = require('pug');
const htmlToText = require('html-to-text')

// new Email(user, url).sendWelcome();

module.exports = class Email {
    constructor(user, url){
        this.to = user.email;
        this.firstName = user.name.split(' ')[0];
        this.url = url;
        this.from = `Leo Shon <${process.env.EMAIL_FROM}>`
    }

    newTransport(){
        if(process.env.NODE_ENV === 'production') {
            return nodemailer.createTransport({
                service: 'SendGrid',
                auth: {
                    user: process.env.SENDGRID_USERNAME,
                    pass: process.env.SENDGRID_PASSWORD,
                }
            })
        }
        return nodemailer.createTransport({
            host: process.env.EMAIL_HOST,
            port: process.env.EMAIL_PORT,
            auth: {
                user: process.env.EMAIL_USERNAME,
                pass: process.env.EMAIL_PASSWORD
            },
            secure: false,
        });
    }
    // Send the actual email 
    async send(template, subject) {
        // 1. render HTML based on a pug tempalte

        const html = pug.renderFile(`${__dirname}/../views/email/${template}.pug`, {
            firstname: this.firstname,
            url: this.url,
            subject
        });

        // 2. Define email options
        const mailOptions = {
            //Dev mode
            // from: this.from,
            //Production mode
            from: process.env.SENDGRID_EMAIL_FROM,
            to: this.to,
            subject,
            html,
            text: htmlToText.convert(html),
        };
        // 3. Create a transport and send email
        await this.newTransport().sendMail(mailOptions)
    }
    async sendWelcome() {
        await this.send('welcome', 'Welcome to the Natours Family!')
    }
    async sendPasswordReset(){
        await this.send('passwordReset', 'Your password reset token (valid for only 10 minutes)')
    }
}

// const sendEmail = async options => {
//     1. Create a transporter
//     const transporter = nodemailer.createTransport({
//         host: process.env.EMAIL_HOST,
//         port: process.env.EMAIL_PORT,
//         auth: {
//             user: process.env.EMAIL_USERNAME,
//             pass: process.env.EMAIL_PASSWORD
//         },
//         secure: false,
//     });
//     2. Define the email options
//         const mailOptions = {
//             from: 'Leo Shon <bruh@bruh.com>',
//             to: options.email,
//             subject: options.subject,
//             text: options.message,
//             // html: 
//         }
//     3. Actually send the email  
//     await transporter.sendMail(mailOptions)
// };

// module.exports = sendEmail;